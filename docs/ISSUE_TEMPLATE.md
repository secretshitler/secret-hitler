## Making an Issue

Thank you for using the issue tracker! Reporting bugs and informing us of new suggestions
this way makes it easier for oversight and allows us to take care of the issue much faster. :smiley:

### Type
- [ ] Bug/Glitch
- [ ] Suggestion

#### Bug/Glitch Reports
**Fill this section for bug/glitch reports.**
*Try to make sure the bug/glitch has not yet been addressed in another Issue.*

##### Bug Details
Please specify the game version at the time the bug occurred:

Please specify what OS you were using at the time the bug occurred:

Please specify what browser you were using at the time the bug occurred:

Please specify what Unique ID the game had at the time the bug occurred (if applicable):

Please specify (if known) the steps to reproduce the bug so we best know how to find a fix:

Please specify any additional information that would prove useful in the resolving of the bug:

#### Suggestion Reports
**Fill this section for suggestion reports.**

To what part of the game does your suggestion apply to?
- [ ] Gameplay
- [ ] User Interface and Graphics
- [ ] Game Modes
- [ ] Accessibility
- [ ] Other

How much effort would implementing your suggestion take? (estimate)
- [ ] Very simple
- [ ] Reasonable effort
- [ ] A lot of work
- [ ] Only for group efforts

Please write your suggestion concisely here:


---

sh.io Issue Template
